package kz.edu.okulik.rest;

import kz.edu.okulik.domain.Feedback;
import kz.edu.okulik.repository.FeedbackRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/api/feed")
@CrossOrigin
public class RestFeedback {

    @Autowired
    private FeedbackRepository feedbackRepository;

    @GetMapping(value = "/all")
    public SecurityContext getList() {
        return SecurityContextHolder.getContext();
    }

    @GetMapping(value = "/{id}")
    public Feedback getOne(@PathVariable Long id) {
        return feedbackRepository.getOne(id);
    }

    @PostMapping
    public ResponseEntity<?> create(@RequestBody Feedback feedback) {
        return new ResponseEntity<Feedback>(feedbackRepository.saveAndFlush(feedback), HttpStatus.CREATED);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<?> delete(@PathVariable Long id) {
        feedbackRepository.deleteById(id);
        return new ResponseEntity<Feedback>(HttpStatus.ACCEPTED);
    }

}
