package kz.edu.okulik.rest;

import kz.edu.okulik.domain.Comment;
import kz.edu.okulik.repository.CommentRepository;
import kz.edu.okulik.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api/comment")
@CrossOrigin
public class RestComment {

    @Autowired
    private CommentRepository commentRepository;
    @Autowired
    private CommentService commentService;

    @GetMapping(value = "/all")
    public List<Comment> getList() {
        return commentRepository.findAll();
    }

    @GetMapping(value = "/{id}")
    public Comment getOne(@PathVariable Long id) {
        return commentRepository.getOne(id);
    }

    @PostMapping(value = "/{postId}")
    public ResponseEntity<?> create(@PathVariable(value = "postId") Long postId, @RequestBody Comment comment) {
        return commentService.save(postId, comment);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity delete(@PathVariable Long id) {
        return commentService.delete(id);
    }
}
