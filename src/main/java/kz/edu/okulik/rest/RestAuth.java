package kz.edu.okulik.rest;


import kz.edu.okulik.domain.User;
import kz.edu.okulik.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@RestController
@RequestMapping(value = "/api/auth")
@CrossOrigin
public class RestAuth {

    public static final Logger logger = LoggerFactory.getLogger(RestAuth.class);

    @Autowired
    private UserService userService;


    @RequestMapping("/login")
    public Principal login(Principal principal) {
        logger.info("user logged " + principal);
        return principal;
    }

    @PostMapping(value = "/register")
    public ResponseEntity<?> register(@RequestBody User newUser) {
        return userService.save(newUser);
    }

    @GetMapping(value = "/getauth")
    public SecurityContext getauth() {
        return SecurityContextHolder.getContext();
    }

}
