package kz.edu.okulik.rest;

import kz.edu.okulik.domain.Post;
import kz.edu.okulik.repository.PostRepository;
import kz.edu.okulik.service.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api/post")
@CrossOrigin
public class RestPost {

    @Autowired
    private PostRepository postRepository;
    @Autowired
    private PostService postService;

    @GetMapping(value = "/all")
    public List<Post> getList() {
        return postRepository.findAll();
    }

    @GetMapping(value = "/{id}")
    public Post getOne(@PathVariable Long id) {
        return postRepository.getOne(id);
    }

    @PostMapping
    public ResponseEntity<?> create(@RequestBody Post post) {
        return postService.save(post);
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<?> update(@RequestBody Post post, @PathVariable Long id) {
        return postService.update(post, id);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity delete(@PathVariable Long id) {
        return postService.delete(id);
    }

}
