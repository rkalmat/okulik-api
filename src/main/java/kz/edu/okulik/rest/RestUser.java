package kz.edu.okulik.rest;

import kz.edu.okulik.domain.User;
import kz.edu.okulik.repository.UserRepository;
import kz.edu.okulik.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api/user")
@CrossOrigin
public class RestUser {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserService userService;

    @GetMapping(value = "/all")
    public List<User> getList() {
        return userRepository.findAll();
    }

    @GetMapping(value = "/{id}")
    public User getOne(@PathVariable Long id) {
        return userRepository.getOne(id);
    }

    @PostMapping
    public ResponseEntity<?> create(@RequestBody User user) {
        return userService.save(user);
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<?> update(@RequestBody User user, @PathVariable Long id) {
        return userService.update(user, id);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity delete(@PathVariable Long id) {
        return userService.delete(id);
    }

    @PostMapping("/change-subscription/{channelId}")
    public User changeSubscription(
            @AuthenticationPrincipal User subscriber,
            @PathVariable("channelId") User channel
    ) {
        if (subscriber.equals(channel)) {
            return channel;
        } else {
            return userService.changeSubscription(channel, subscriber);
        }
    }

}
