package kz.edu.okulik.rest;

import kz.edu.okulik.domain.Question;
import kz.edu.okulik.pojo.QuestionFilter;
import kz.edu.okulik.repository.QuestionRepository;
import kz.edu.okulik.service.QuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api/question")
@CrossOrigin
public class RestQuestion {

    @Autowired
    private QuestionRepository questionRepository;
    @Autowired
    private QuestionService questionService;

    @GetMapping(value = "/all")
    public List<Question> getList() {
        return questionRepository.findAll();
    }

    @GetMapping(value = "/{id}")
    public Question getOne(@PathVariable Long id) {
        return questionRepository.getOne(id);
    }

    @GetMapping(value = "/filter")
    public List<Question> getByFilter(@RequestBody QuestionFilter questionFilter) {
        return questionService.getByFilter(questionFilter);
    }
}
