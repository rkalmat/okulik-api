package kz.edu.okulik.service;

import kz.edu.okulik.domain.Discussion;
import org.springframework.http.ResponseEntity;

public interface DiscussionService {

    ResponseEntity<Discussion> save(Discussion discussion);

    ResponseEntity update(Discussion discussion, Long id);

    ResponseEntity delete(Long id);
}
