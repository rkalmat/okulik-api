package kz.edu.okulik.service.impl;

import kz.edu.okulik.domain.Comment;
import kz.edu.okulik.domain.Post;
import kz.edu.okulik.domain.enums.Role;
import kz.edu.okulik.domain.User;
import kz.edu.okulik.repository.CommentRepository;
import kz.edu.okulik.repository.PostRepository;
import kz.edu.okulik.repository.UserRepository;
import kz.edu.okulik.rest.RestAuth;
import kz.edu.okulik.service.CommentService;
import kz.edu.okulik.util.CustomErrorType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
public class CommentServiceImpl implements CommentService {
    @Autowired
    private CommentRepository commentRepository;

    @Autowired
    private PostRepository postRepository;

    @Autowired
    private UserRepository userRepository;

    public static final Logger logger = LoggerFactory.getLogger(RestAuth.class);

    @Override
    public ResponseEntity<Comment> save(Long postId, Comment comment) {

        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        comment.setUser(user);

        comment.setPost(postRepository.getOne(postId));

        return new ResponseEntity<Comment>(commentRepository.saveAndFlush(comment), HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity delete(Long id) {

        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (commentRepository.getOne(id).getUser().equals(user)
                || user.getRoles().contains(Role.ADMIN)) {
            commentRepository.deleteById(id);
            return new ResponseEntity<Post>(HttpStatus.ACCEPTED);
        } else {
            logger.error("Delete error " + postRepository.getOne(id).getTitle());
            return new ResponseEntity(
                    new CustomErrorType("Error while deleting post"),
                    HttpStatus.CONFLICT);
        }
    }
}
