package kz.edu.okulik.service.impl;

import kz.edu.okulik.pojo.UserValidate;
import kz.edu.okulik.repository.UserRepository;
import kz.edu.okulik.service.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

@Service
public class AuthServiceImpl implements AuthService {

    private UserValidate userValidate;

    @Autowired
    private UserRepository userRepository;

    @Override
    public boolean validate(String authorizationHeader) {

        userValidate = getUser(authorizationHeader);

        if (userRepository.findByUsernameAndPassword(userValidate.getUsername(), userValidate.getPassword()) != null) {
            return true;
        } else return false;
    }

    public UserValidate getUser(String authorizationHeader) {
        if (authorizationHeader != null && authorizationHeader.toLowerCase().startsWith("basic")) {
            // Authorization: Basic base64credentials
            String base64Credentials = authorizationHeader.substring("Basic".length()).trim();
            byte[] credDecoded = Base64.getDecoder().decode(base64Credentials);
            String credentials = new String(credDecoded, StandardCharsets.UTF_8);
            // credentials = username:password
            String[] values = credentials.split(":", 2);

            return new UserValidate(values[0], values[1]);
        }
        return null;
    }
}
