package kz.edu.okulik.service.impl;

import kz.edu.okulik.domain.enums.Role;
import kz.edu.okulik.domain.User;
import kz.edu.okulik.repository.UserRepository;
import kz.edu.okulik.rest.RestAuth;
import kz.edu.okulik.service.UserService;
import kz.edu.okulik.util.CustomErrorType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class UserServiceImpl implements UserService {

    public static final Logger logger = LoggerFactory.getLogger(RestAuth.class);

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return userRepository.findByUsername(username);
    }

    @Override
    @SuppressWarnings("unchecked")
    public ResponseEntity save(User newUser) {
        if (userRepository.findByUsername(newUser.getUsername()) != null) {

            logger.error("username Already exist " + newUser.getUsername());
            return new ResponseEntity(
                    new CustomErrorType("user with username " + newUser.getUsername() + "already exist "),
                    HttpStatus.CONFLICT);
        }
        Set<Role> roles = new HashSet<>();
        roles.add(Role.USER);
        newUser.setRoles(roles);

        return new ResponseEntity<User>(userRepository.saveAndFlush(newUser), HttpStatus.CREATED);
    }

    @Override
    @SuppressWarnings("unchecked")
    public ResponseEntity update(User user, Long id) {
        User authUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (userRepository.getOne(id).equals(authUser) ||
                authUser.getRoles().contains(Role.ADMIN)) {

            User userToUpdate = userRepository.getOne(id);
            userToUpdate.setUsername(user.getUsername());
            userToUpdate.setPassword(user.getPassword());
            userToUpdate.setFirstname(user.getFirstname());
            userToUpdate.setLastname(user.getLastname());
            userToUpdate.setEmail(user.getEmail());
            userToUpdate.setRoles(user.getRoles());
            userToUpdate.setActive(user.isActive());

//            userRepository.save(userToUpdate);
            return new ResponseEntity<User>(userRepository.saveAndFlush(userToUpdate), HttpStatus.ACCEPTED);
        } else {
            logger.error("Update error " + user.getUsername());
            return new ResponseEntity(
                    new CustomErrorType("Error while updating user"),
                    HttpStatus.CONFLICT);
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public ResponseEntity delete(Long id) {
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (user.getRoles().contains(Role.ADMIN)) {
            userRepository.deleteById(id);
            return new ResponseEntity<User>(HttpStatus.ACCEPTED);
        } else {
            logger.error("Delete error " + userRepository.getOne(id).getUsername());
            return new ResponseEntity(
                    new CustomErrorType("Error while deleting user"),
                    HttpStatus.CONFLICT);
        }
    }

    @Override
    public User changeSubscription(User channel, User subscriber) {
        Set<User> subscribers = channel.getSubscribers();

        if (subscribers.contains(subscriber)) {
            subscribers.remove(subscriber);
        } else {
            subscribers.add(subscriber);
        }

        return userRepository.save(channel);
    }
}
