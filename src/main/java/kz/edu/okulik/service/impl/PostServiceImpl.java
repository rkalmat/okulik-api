package kz.edu.okulik.service.impl;

import kz.edu.okulik.domain.Post;
import kz.edu.okulik.domain.enums.Role;
import kz.edu.okulik.domain.User;
import kz.edu.okulik.repository.PostRepository;
import kz.edu.okulik.rest.RestAuth;
import kz.edu.okulik.service.PostService;
import kz.edu.okulik.util.CustomErrorType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
public class PostServiceImpl implements PostService {
    @Autowired
    private PostRepository postRepository;

    public static final Logger logger = LoggerFactory.getLogger(RestAuth.class);

    @Override
    public ResponseEntity<Post> save(Post post) {

        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        post.setUser(user);

        return new ResponseEntity<Post>(postRepository.saveAndFlush(post), HttpStatus.CREATED);
    }

    @Override
    @SuppressWarnings("unchecked")
    public ResponseEntity update(Post post, Long id) {
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (postRepository.getOne(id).getUser().equals(user) ||
                user.getRoles().contains(Role.ADMIN)) {

            Post postToUpdate = postRepository.getOne(id);
            postToUpdate.setTitle(post.getTitle());
            postToUpdate.setText(post.getText());

//            postRepository.save(postToUpdate);
            return new ResponseEntity<Post>(postRepository.saveAndFlush(postToUpdate), HttpStatus.ACCEPTED);
        } else {
            logger.error("Update error " + post.getTitle());
            return new ResponseEntity(
                    new CustomErrorType("Error while updating post"),
                    HttpStatus.CONFLICT);
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public ResponseEntity delete(Long id) {

        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (postRepository.getOne(id).getUser().equals(user)
                || user.getRoles().contains(Role.ADMIN)) {
            postRepository.deleteById(id);
            return new ResponseEntity<Post>(HttpStatus.ACCEPTED);
        } else {
            logger.error("Delete error " + postRepository.getOne(id).getTitle());
            return new ResponseEntity(
                    new CustomErrorType("Error while deleting post"),
                    HttpStatus.CONFLICT);
        }
    }
}
