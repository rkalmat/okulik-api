package kz.edu.okulik.service.impl;

import kz.edu.okulik.domain.Question;
import kz.edu.okulik.pojo.QuestionFilter;
import kz.edu.okulik.pojo.enums.GenerateType;
import kz.edu.okulik.repository.GradeRepository;
import kz.edu.okulik.repository.ParagraphRepository;
import kz.edu.okulik.repository.QuestionRepository;
import kz.edu.okulik.repository.SectionRepository;
import kz.edu.okulik.service.QuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

@Service
public class QuestionServiceImpl implements QuestionService {

    @Autowired
    private QuestionRepository questionRepository;

    @Autowired
    private ParagraphRepository paragraphRepository;

    @Autowired
    private SectionRepository sectionRepository;

    @Autowired
    private GradeRepository gradeRepository;

    @Override
    public ResponseEntity<Question> save(Question question) {
        return null;
    }

    @Override
    public ResponseEntity update(Question question, Long id) {
        return null;
    }

    @Override
    public ResponseEntity delete(Long id) {
        return null;
    }

    @Override
    public List<Question> getByFilter(QuestionFilter questionFilter) {

        List<Question> questions;

        if(questionFilter.getGrade() != null){
            if(questionFilter.getSection() != null){
                if(questionFilter.getParagraph() != null){
                    questions = questionRepository.findByParagraph(paragraphRepository.getOne(Long.parseLong(questionFilter.getParagraph())));
                }else {
                    questions = questionRepository.findBySection(sectionRepository.getOne(Long.parseLong(questionFilter.getSection())));
                }
            }else {
                questions = questionRepository.findByGrade(gradeRepository.getOne(Long.parseLong(questionFilter.getGrade())));
            }
        }else{
            questions = questionRepository.findAll();
        }

        if(questionFilter.getGenerateType().equals(GenerateType.RANDOM) && questions.size() > 30){
            Collections.shuffle(questions);
            questions = questions.subList(0,30);
        }

        return questions;
    }
}
