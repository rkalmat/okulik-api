package kz.edu.okulik.service.impl;

import kz.edu.okulik.domain.Discussion;
import kz.edu.okulik.domain.Rate;
import kz.edu.okulik.domain.User;
import kz.edu.okulik.domain.enums.Role;
import kz.edu.okulik.repository.DiscussionRepository;
import kz.edu.okulik.rest.RestAuth;
import kz.edu.okulik.service.DiscussionService;
import kz.edu.okulik.util.CustomErrorType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;

public class DiscussionServiceImpl implements DiscussionService {

    @Autowired
    private DiscussionRepository discussionRepository;

    public static final Logger logger = LoggerFactory.getLogger(RestAuth.class);

    @Override
    public ResponseEntity<Discussion> save(Discussion discussion) {
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        discussion.setUser(user);
        discussion.setRate(new Rate());

        return new ResponseEntity<Discussion>(discussionRepository.saveAndFlush(discussion), HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity update(Discussion discussion, Long id) {
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (discussionRepository.getOne(id).getUser().equals(user) ||
                user.getRoles().contains(Role.ADMIN)) {

            Discussion discussionToUpdate = discussionRepository.getOne(id);
            discussionToUpdate.setTitle(discussion.getTitle());
            discussionToUpdate.setContent(discussion.getContent());

            return new ResponseEntity<Discussion>(discussionRepository.saveAndFlush(discussionToUpdate), HttpStatus.ACCEPTED);
        } else {
            logger.error("Update error " + discussion.getTitle());
            return new ResponseEntity(
                    new CustomErrorType("Error while updating discussion"),
                    HttpStatus.CONFLICT);
        }
    }

    @Override
    public ResponseEntity delete(Long id) {
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (discussionRepository.getOne(id).getUser().equals(user)
                || user.getRoles().contains(Role.ADMIN)) {
            discussionRepository.deleteById(id);
            return new ResponseEntity<Discussion>(HttpStatus.ACCEPTED);
        } else {
            logger.error("Delete error " + discussionRepository.getOne(id).getTitle());
            return new ResponseEntity(
                    new CustomErrorType("Error while deleting discussion"),
                    HttpStatus.CONFLICT);
        }
    }
}
