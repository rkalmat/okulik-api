package kz.edu.okulik.service;

import kz.edu.okulik.domain.Post;
import org.springframework.http.ResponseEntity;

public interface PostService {

    ResponseEntity<Post> save(Post post);

    ResponseEntity update(Post post, Long id);

    ResponseEntity delete(Long id);
}
