package kz.edu.okulik.service;

import kz.edu.okulik.domain.Comment;
import org.springframework.http.ResponseEntity;

public interface CommentService {

    ResponseEntity<Comment> save(Long postId, Comment comment);

    ResponseEntity delete(Long id);
}
