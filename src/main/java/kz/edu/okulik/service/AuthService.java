package kz.edu.okulik.service;

import kz.edu.okulik.pojo.UserValidate;

public interface AuthService {

    boolean validate(String authorizationHeader);

    UserValidate getUser(String authorizationHeader);

}
