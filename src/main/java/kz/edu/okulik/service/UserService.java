package kz.edu.okulik.service;

import kz.edu.okulik.domain.User;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface UserService extends UserDetailsService {

    ResponseEntity save(User newUser);

    ResponseEntity update(User user, Long id);

    ResponseEntity delete(Long id);

    User changeSubscription(User channel, User subscriber);
}
