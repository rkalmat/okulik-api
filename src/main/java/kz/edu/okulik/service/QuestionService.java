package kz.edu.okulik.service;

import kz.edu.okulik.domain.Question;
import kz.edu.okulik.pojo.QuestionFilter;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface QuestionService {

    ResponseEntity<Question> save(Question question);

    ResponseEntity update(Question question, Long id);

    ResponseEntity delete(Long id);

    List<Question> getByFilter(QuestionFilter questionFilter);
}
