package kz.edu.okulik.domain;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Entity
@Table
public class Rate {

    @Id
    @GeneratedValue(generator = "rate_seq", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(sequenceName = "rate_seq_id", name = "rate_seq",
            allocationSize = 1)
    private Long id;

    private int like = 0;
    private int dislike = 0;
    private int views = 0;

    private LocalDateTime createdDate = LocalDateTime.now();

}
