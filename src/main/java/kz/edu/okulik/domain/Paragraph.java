package kz.edu.okulik.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table
public class Paragraph {

    @Id
    @GeneratedValue(generator = "paragraph_seq", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(sequenceName = "paragraph_seq_id", name = "paragraph_seq",
            allocationSize = 1)
    private Long id;

    private String title;

    @JsonIgnore
    @OneToMany(
            mappedBy = "paragraph",
            cascade = CascadeType.REMOVE,
            orphanRemoval = true
    )
    private List<Question> questions;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "section_id", foreignKey = @ForeignKey(name = "paragraph_section_fk"))
    private Section section;
}
