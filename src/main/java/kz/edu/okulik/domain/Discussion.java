package kz.edu.okulik.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table
public class Discussion {

    @Id
    @GeneratedValue(generator = "discussion_seq", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(sequenceName = "discussion_seq_id", name = "discussion_seq",
            allocationSize = 1)
    private Long id;

    private String title;

    private String content;

    @OneToOne(mappedBy = "user", cascade = CascadeType.ALL)
    @JoinColumn(name = "rate_id", referencedColumnName = "id")
    private Rate rate;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", foreignKey = @ForeignKey(name = "discussion_user_fk"))
    private User user;
}
