package kz.edu.okulik.domain;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table
public class Feedback {

    @Id
    @GeneratedValue(generator = "feedback_seq", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(sequenceName = "feedback_seq_id", name = "feedback_seq",
            allocationSize = 1)
    private Long id;

    private String title;

    private String email;

    private String message;

}
