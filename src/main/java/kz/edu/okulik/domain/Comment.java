package kz.edu.okulik.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

@Data
@Entity
@Table
public class Comment {
    @Id
    @GeneratedValue(generator = "comment_seq", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(sequenceName = "comment_seq_id", name = "comment_seq",
            allocationSize = 1)
    private Long id;

    private String text;

    @JsonIgnore
    @ManyToOne(optional = false)
    @JoinColumn(name="user_id", foreignKey = @ForeignKey(name = "comment_user_fk"))
    private User user;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "post_id", foreignKey = @ForeignKey(name = "comment_post_fk"), nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Post post;

}
