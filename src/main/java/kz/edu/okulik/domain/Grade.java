package kz.edu.okulik.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table
public class Grade {

    @Id
    @GeneratedValue(generator = "grade_seq", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(sequenceName = "grade_seq_id", name = "grade_seq",
            allocationSize = 1)
    private Long id;

    private String description;

    @JsonIgnore
    @OneToMany(
            mappedBy = "grade",
            cascade = CascadeType.REMOVE,
            orphanRemoval = true
    )
    private List<Section> sections;
}
