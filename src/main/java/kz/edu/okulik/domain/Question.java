package kz.edu.okulik.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table
public class Question {

    @Id
    @GeneratedValue(generator = "question_seq", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(sequenceName = "question_seq_id", name = "question_seq",
            allocationSize = 1)
    private Long id;

    private String text;

    private String errorAnswer1;
    private String errorAnswer2;
    private String errorAnswer3;
    private String errorAnswer4;
    private String rightAnswer;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "paragraph_id", foreignKey = @ForeignKey(name = "question_paragraph_fk"))
    private Paragraph paragraph;
}
