package kz.edu.okulik.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table
public class Section {

    @Id
    @GeneratedValue(generator = "section_seq", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(sequenceName = "section_seq_id", name = "section_seq",
            allocationSize = 1)
    private Long id;

    private String title;

//    @JsonIgnore
//    @OneToMany(
//            mappedBy = "section",
//            cascade = CascadeType.REMOVE,
//            orphanRemoval = true
//    )
//    private List<Paragraph> paragraphs;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "grade_id", foreignKey = @ForeignKey(name = "section_grade_fk"))
    private Grade grade;
}
