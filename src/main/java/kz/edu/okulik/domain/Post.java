package kz.edu.okulik.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table
public class Post {

    @Id
    @GeneratedValue(generator = "post_seq", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(sequenceName = "post_seq_id", name = "post_seq",
            allocationSize = 1)
    private Long id;

    private String title;

    private String text;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", foreignKey = @ForeignKey(name = "post_user_fk"))
    private User user;

}
