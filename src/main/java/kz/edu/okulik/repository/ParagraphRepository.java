package kz.edu.okulik.repository;

import kz.edu.okulik.domain.Paragraph;
import kz.edu.okulik.domain.Question;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ParagraphRepository extends JpaRepository<Paragraph, Long> {

    List<Question> findBySection(Long id);
}
