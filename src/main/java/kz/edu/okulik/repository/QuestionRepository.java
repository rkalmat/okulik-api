package kz.edu.okulik.repository;

import kz.edu.okulik.domain.Grade;
import kz.edu.okulik.domain.Paragraph;
import kz.edu.okulik.domain.Question;
import kz.edu.okulik.domain.Section;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface QuestionRepository extends JpaRepository<Question, Long> {

    List<Question> findByParagraph(Paragraph paragraph);

    @Query("SELECT q FROM Question q " +
            "JOIN q.paragraph p " +
            "WHERE p.section =:section")
    List<Question> findBySection(@Param("section") Section section);

    @Query("SELECT q FROM Question q " +
            "JOIN q.paragraph p " +
            "JOIN p.section s " +
            "WHERE s.grade = :grade")
    List<Question> findByGrade(@Param("grade") Grade grade);
}
