package kz.edu.okulik.repository;

import kz.edu.okulik.domain.User;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    User findByUsername(String username);

    User findByUsernameAndPassword(String username, String password);

    @EntityGraph(attributePaths = { "subscriptions", "subscribers" })
    Optional<User> findById(Long id);

}
