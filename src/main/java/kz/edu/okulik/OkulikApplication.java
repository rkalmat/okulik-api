package kz.edu.okulik;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OkulikApplication {

	public static void main(String[] args) {
		SpringApplication.run(OkulikApplication.class, args);
	}

}
