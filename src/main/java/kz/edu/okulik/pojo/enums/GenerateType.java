package kz.edu.okulik.pojo.enums;

public enum GenerateType {
    ALL,
    RANDOM;
}
