package kz.edu.okulik.pojo;

import kz.edu.okulik.pojo.enums.GenerateType;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class QuestionFilter {

    private String grade;
    private String section;
    private String paragraph;
    private GenerateType generateType;

}
