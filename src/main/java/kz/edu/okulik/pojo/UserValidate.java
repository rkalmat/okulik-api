package kz.edu.okulik.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class UserValidate {

    private String username;
    private String password;

}
